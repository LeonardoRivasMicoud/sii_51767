// Mundo.cpp: implementation of the CMundo class.
//Este fichero pertenece  a LEONARDO

////////////////////////////////////////////////////////////////////
#pragma once
#include <fstream>
#include "mundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <unistd.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{	
	char cadena[30];
	sprintf(cadena,"@ game over\n"); //mensaje de fin de juego
	
	//cerramos el bot
	pdatos->accion=3;
	munmap(proyeccion,sizeof(datos));
	unlink("/tmp/bot.txt");

	close(fd_coord);
	close(fd_tcl);
	unlink("/tmp/fifoCoord");
	unlink("/tmp/fifoTeclas");

}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	pdatos->esfera=esfera;			//ASignación de informacion
	pdatos->raqueta1=jugador2;
	

	switch(pdatos->accion)    
	{
		case 1: OnKeyboardDown( 'l',0,0); break;
		case 0: break;
		case -1:OnKeyboardDown( 'o',0,0); break;
	}
	

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	if(jugador1.Rebota(esfera)) 		// ESTO ES COSECHA PROPIA
	jugador2.velocidad.y=0;

	if(jugador2.Rebota(esfera))
	jugador1.velocidad.y=0;

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		
		
	}



	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
	
		
	}

	//Leer datos de la tubería de coordenadas
	read(fd_coord,cad,sizeof(cad));
		
	if(cad[0]=='@')exit(1);
	//Actualizar los atributos de MundoCliente respecto a mundo servidor

	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 


}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}*/

	//Le enviamos la tecla seleccionada al servidor por la tuberia de teclas

	sprintf(cad_tcl,"%c",key);
	write(fd_tcl,cad_tcl,strlen(cad_tcl)+1);

}

void CMundoCliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
/*	
//LOguer
	fd=open("/tmp/myfifo", O_WRONLY);
	if (fd<0)exit(3);			*/

//BOT

	fbot=open("/tmp/bot.txt",O_RDWR|O_CREAT|O_TRUNC,0777);
	write(fbot,&datos,sizeof(datos));
	proyeccion=(char*)mmap(NULL,sizeof(datos),PROT_WRITE|PROT_READ,MAP_SHARED,fbot,0);
	close(fbot);
	pdatos=(DatosMemCompartida*)proyeccion;
	pdatos->accion=0;
	
//CREO FIFO COORDENADAS Y teclas
	unlink("/tmp/fifoCoord"); //Por si no se cerro bien
	unlink("/tmp/fifoTeclas");
	mkfifo("/tmp/fifoCoord",0777);  //coordenadas
	mkfifo("/tmp/fifoTeclas",0777); //teclas



//LAS ABRIMOS

fd_coord=open("/tmp/fifoCoord",O_RDONLY);
	
	if(fd_coord==-1){
		perror("Error al abrir la tubería de coord\n");
		exit(1);
	}
	



fd_tcl=open("/tmp/fifoTeclas",O_WRONLY);
	if(fd_tcl==-1){
		printf("Error al abrir la tubería de  tcl\n");
		exit(1);
	}
	

	
}
